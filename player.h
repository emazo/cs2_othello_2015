#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <utility>

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
   	Board *board;

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Side side;
    std::vector<Move*> *neighbors;
    int depth;
    void init_for_testminimax();
	std::vector<Move*> *findMoves(std::vector<Move*> *tempneighbors, std::vector<Move*> *moves, Board *b, Side side);
	int heuristic(Move *move, Side side, Board *b);
	std::vector<Move*>* Neighbors(Move *move, Board *b);
	std::pair<Move*, int> MaxCount(std::vector<std::pair<Move*, int> > * v);
    Move *moveHelper(Move *opponentsMove, Board *board3, int depth, Side side);


};

#endif
