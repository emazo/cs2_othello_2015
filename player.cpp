#include "player.h"
#include "board.h"
#include <vector>
#include <limits.h>
#include <utility>

#define MAX_DEPTH 3
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

     // Set up a new board
     // What other initializatoin do I need to do? 
     Board *brd = new Board(); 
     this->board = brd;
     this->side = side;
     

     
     neighbors = new std::vector<Move*>(); 
     //Move (4, 3), (5, 3), (6, 4), (6, 5), (3, 4), (3, 5), (3, 6), (6, 6), (4, 6), (5, 6);
     neighbors->push_back(new Move (3, 2));
     neighbors->push_back(new Move (4, 2));
     neighbors->push_back(new Move (5, 3));
     neighbors->push_back(new Move (5, 4));
     neighbors->push_back(new Move (2, 3));
     neighbors->push_back(new Move (2, 4));
     neighbors->push_back(new Move (2, 5));
     neighbors->push_back(new Move (5, 5));
     neighbors->push_back(new Move (3, 5));
     neighbors->push_back(new Move (4, 5));

     //*********
     neighbors->push_back(new Move (2, 2));
     neighbors->push_back(new Move (5, 2));
     //*********

     std::cerr << "init" << std::endl;
     this->depth = 0;
     

}

void Player::init_for_testminimax() {
    //hard code neighbors fors testminimax moves
    neighbors->clear();
    neighbors->push_back(new Move (1, 1));
    neighbors->push_back(new Move (6, 3));

}


//First commit1!1
/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */

std::vector<Move*> *Player::findMoves(std::vector<Move*> *tempneighbors, std::vector<Move*> *moves, Board *board3, Side side){
    std::cerr << "In findMoves: current size of neighbors is " << tempneighbors->size() << std::endl;
    for (int j = 0; j < 8; j++)
            {
            for (int i = 0; i < 8; i++)
                {
                    //std::cerr << "checking" << std::endl;
                    Move *myMove = new Move(j, i);
                    if (board3->checkMove(myMove, side))
                    {
                        std::cerr << "Found a Valid Move" << std::endl;
                        moves->push_back(myMove);
                        std::cerr << myMove->x << std::endl;
                        std::cerr << myMove->y << std::endl;
                    }
                }
            }
        
        std::cerr << "return findMoves" << std::endl;
        return moves;
}

//Change all *= to +=

int Player::heuristic(Move *move, Side side, Board *b){
        int score;
        Board *board2 = b->copy();
        board2->doMove(move, side); // Do the move for myself to keep track
        int mySide = board2->count(side);
        //int yourSide = board2->count(other);
        int X = (move)->x;
        int Y = (move)->y;
        if (X == 0 || X == 7)
        {
            if (Y == 0 || Y == 7)
            {
                // Then corner piece, * 3
                mySide += 3;  
            }
            else 
            {
                // Then side piece * 2
                // Check if "corner giving " piece
                if (Y == 1 || Y == 6)
                {
                    // * -3
                    mySide += -3;
                }
                else 
                {
                    mySide += 2;
                }
            }
        }
        if (Y == 0 || Y == 7)
        {
            // Then top or bottom piece
            // Check if "corner giving " piece
            if (X == 1 || X == 6)
                {
                    // * -3
                    mySide += -3;
                }
        }
        score = mySide;
        std::cerr << "return heuristic" << std::endl;
        return score;
}

// Changed all neighbors inside the code to tempneighbors
std::vector<Move*> *Player::Neighbors(Move *move, Board *b)
{
    //*************
    std::vector<Move*> *tempneighbors = new std::vector<Move*> (); ;
    for (int num = 0; num < neighbors->size(); num++) {
        tempneighbors->push_back(neighbors->at(num));
    }
    //************* Need to check that this only passes values


        if (move->x == 0 && move-> y == 0)
        {
            Move *i = new Move(move->x + 1, move->y);
            Move *d = new Move(move->x, move->y + 1);
            Move *g = new Move(move->x + 1, move->y + 1);

            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(g->x, g->y))
            {
                tempneighbors->push_back(new Move (g->x, g->y));
            }
        }
        else if (move->x == 7 && move-> y == 0)
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *d = new Move(move->x, move->y + 1);
            Move *e = new Move(move->x - 1, move->y + 1);

            if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(e->x, e->y))
            {
                tempneighbors->push_back(new Move (e->x, e->y));
            }
        }
        else if (move->x == 7 && move-> y == 7)
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *f = new Move(move->x - 1, move->y - 1);

            if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(f->x, f->y))
            {
                tempneighbors->push_back(new Move (f->x, f->y));
            }
        }
        else if (move->x == 0 && move->y == 7)
        {
            Move *i = new Move(move->x + 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *h = new Move(move->x + 1, move->y - 1);

            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(h->x, h->y))
            {
                tempneighbors->push_back(new Move (h->x, h->y));
            }
        }
        else if (move->x + 1 > 7)
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *d = new Move(move->x, move->y + 1);
            Move *e = new Move(move->x - 1, move->y + 1);
            Move *f = new Move(move->x - 1, move->y - 1);

            if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(e->x, e->y))
            {
                tempneighbors->push_back(new Move (e->x, e->y));
            }
            if (!b->occupied(f->x, f->y))
            {
                tempneighbors->push_back(new Move (f->x, f->y));
            }
        }
        else if (move->x - 1 < 0)
        {
            Move *i = new Move(move->x + 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *d = new Move(move->x, move->y + 1);
            Move *g = new Move(move->x + 1, move->y + 1);
            Move *h = new Move(move->x + 1, move->y - 1);

            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(g->x, g->y))
            {
                tempneighbors->push_back(new Move (g->x, g->y));
            }
            if (!b->occupied(h->x, h->y))
            {
                tempneighbors->push_back(new Move (h->x, h->y));
            }
        }
        else if (move->y + 1 > 7)
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *i = new Move(move->x + 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *f = new Move(move->x - 1, move->y - 1);
            Move *h = new Move(move->x + 1, move->y - 1);

            if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(f->x, f->y))
            {
                tempneighbors->push_back(new Move (f->x, f->y));
            }
            if (!b->occupied(h->x, h->y))
            {
                tempneighbors->push_back(new Move (h->x, h->y));
            }
        }
        else if (move->y - 1 < 0)
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *i = new Move(move->x + 1, move->y);
            Move *d = new Move(move->x, move->y + 1);
            Move *e = new Move(move->x - 1, move->y + 1);
            Move *g = new Move(move->x + 1, move->y + 1);
             if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(e->x, e->y))
            {
                tempneighbors->push_back(new Move (e->x, e->y));
            }
            if (!b->occupied(g->x, g->y))
            {
                tempneighbors->push_back(new Move (g->x, g->y));
            }
        }
        else 
        {
            Move *a = new Move(move->x - 1, move->y);
            Move *i = new Move(move->x + 1, move->y);
            Move *c = new Move(move->x, move->y - 1);
            Move *d = new Move(move->x, move->y + 1);
            Move *e = new Move(move->x - 1, move->y + 1);
            Move *g = new Move(move->x + 1, move->y + 1);
            Move *f = new Move(move->x - 1, move->y - 1);
            Move *h = new Move(move->x + 1, move->y - 1);

             if (!b->occupied(a->x, a->y))
            {
                tempneighbors->push_back(new Move (a->x, a->y));
            }
            if (!b->occupied(i->x, i->y))
            {
                tempneighbors->push_back(new Move (i->x, i->y));
            }
            if (!b->occupied(c->x, c->y))
            {
                tempneighbors->push_back(new Move (c->x, c->y));
            }
            if (!b->occupied(d->x, d->y))
            {
                tempneighbors->push_back(new Move (d->x, d->y));
            }
            if (!b->occupied(e->x, e->y))
            {
                tempneighbors->push_back(new Move (e->x, e->y));
            }
            if (!b->occupied(g->x, g->y))
            {
                tempneighbors->push_back(new Move (g->x, g->y));
            }
            if (!b->occupied(f->x, f->y))
            {
                tempneighbors->push_back(new Move (f->x, f->y));
            }
            if (!b->occupied(h->x, h->y))
            {
                tempneighbors->push_back(new Move (h->x, h->y));
            }
        }
    return tempneighbors;
}

std::pair<Move*, int> Player::MaxCount(std::vector<std::pair<Move*, int> > *v){
    //std::pair<Move*, int> *newPair = new std::pair<Move*, int>;
    int count = INT_MAX;

    Move *maxi;
    for (int k = 0; k < v->size(); k++)
    {
        if (v->at(k).second < count)
        {
            count = v->at(k).second;
            maxi = v->at(k).first;
        }
    }
    //std::pair <Move *,int> newPair;
    //pair< Move*, int > p;
    //p = (maxi, count);
    //newPair = (maxi,count);    
     //newPair = std::make_pair(max, count);
    std::pair <Move*, int> newPair (maxi, count);
    return newPair;
}

Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's move before calculating your own move
     */ 

     Side other = (side == BLACK) ? WHITE : BLACK;
     Board *b = this->board;
     Move *goodMove;
     //*********
     //int count = INT_MIN;
     //*******
     Board *board3 = b->copy();
     std::cerr << b->countBlack() << std::endl;
     std::cerr << b->countWhite() << std::endl;

     goodMove = moveHelper(opponentsMove, board3, 0, side);
     if (opponentsMove != NULL){
        b->doMove(opponentsMove, other);
     }
     if (goodMove != NULL){
        b->doMove(goodMove, side);
     }

    //Process your opponent's move and update the board
    
    return goodMove;
}

Move *Player::moveHelper(Move *opponentsMove, Board *board3, int depth, Side side){
    std::vector<Move*> *neighbors = this->neighbors;
    std::vector<Move *> *moves = new std::vector<Move *>(); 
    Side other = (side == BLACK) ? WHITE : BLACK;
    std::vector<std::pair<Move*, int> > *v = new std::vector<std::pair<Move*, int> >;

    if (opponentsMove != NULL) //
    {
        board3->doMove(opponentsMove, other); // This doesn't affect b...
        

        /*********************/
        //b = this->board; //ADDED THIS LINE.  Updates the local b to be the current board
        /*********************/

        //This one does actually update the real neighbors because the doMove above is a real move
        //neighbors = Neighbors(opponentsMove, board3); //
    }

     std::cerr << board3->countBlack() << std::endl;
     std::cerr << board3->countWhite() << std::endl;


    if (!board3->isDone()) // If game isn't over
    {
        if (board3->hasMoves(side)) //If there are possible moves for player's side
        {
            moves = findMoves(neighbors, moves, board3, side); //Strange fields to pass it, but works
            std::cerr << "found moves" << std::endl;

            for (int i = 0; i < moves->size(); i++)
            {
                //board3->doMove(moves->at(i), side); 

                std::cerr << "found neighbors" << std::endl;
                Board *board4 = board3->copy();

                //*****************
                //this->depth += 1; Wrong place to update depth
                //*****************
                std::cerr << "Depth is currently " << depth << std::endl;
                if (depth != MAX_DEPTH)
                {
                    std::cerr<< "oppent going" << std::endl;
                    Move *opponentGoes = this->moveHelper(moves->at(i), board4, depth + 1, other); //
                    std::cerr << "opponent Goes" << std::endl;
                    if (opponentGoes != NULL){
                        int score = heuristic(opponentGoes, other, board4);
                        std::pair <Move*, int> opponentPair (moves->at(i), score);
                        v->push_back(opponentPair);
                    }
                }
                else if (depth == MAX_DEPTH)
                {
                    std::cerr << "bottom ply" << std::endl;
                    int score = heuristic(moves->at(i), side, board3);
                    std::cerr << "runs heuristic" << std::endl;
                    std::cerr << moves->at(i)->x << std::endl;
                    std::cerr << moves->at(i)->y << std::endl;
                    std::cerr << score << std::endl;
                    std::pair <Move*, int> opponentPair1 (moves->at(i), score);
                    std::cerr << "makes pair" << std::endl;
                    v->push_back(opponentPair1);
                    std::cerr << "pushed back to v" << std::endl;
                    //return (moves->at(i));
                }
            }
            std::pair <Move*, int> maxPair;
            maxPair =  MaxCount(v);
            Move *return_move = new Move(maxPair.first->x, maxPair.first->y); 
            
            //******************
            //neighbors = Neighbors(return_move, b);
            //******************
            return return_move;
        }
    }
    return NULL;
}
