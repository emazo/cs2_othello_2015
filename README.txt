I've been working solo, so I've done all of this coding myself. My original strategy was to improve the minimax code from week 1 to extend to a general depth (which I can set, instead of hard-coding), and improve my scoring heuristic to increase the "skill" of my minimax algorithm to choose the correct branch. To play in the tournament, I will test what ply (depth) I can extend my minimax to in order to improve gameplay but not go over time. 

Strategies I tried to implement but did not work in time: 
Alpha-Beta Pruning:
	I was having so much difficulty making my minimax algorithm recurse to a general depth, that my attempts to make my mimimax more efficient using alpha-beta pruning ended up taking too much time. It is really hard to code all of this on your own!

Iterative Deepening:
	Dynamically changing the depth to which I run the minimax function will work much better than running it to a pre-determined depth. This is my goal for this week's assignment. 

